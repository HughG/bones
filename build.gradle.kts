import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.21"
}

group = "org.tameter"
version = "0.1"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("junit:junit:4.12")
    implementation("org.hamcrest:hamcrest-core:1.3")
    implementation("org.hamcrest:hamcrest-library:1.3")
//    implementation("com.natpryce:hamkrest:1.7.0.0")
    implementation("org.mockito:mockito-core:1.10.19")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}