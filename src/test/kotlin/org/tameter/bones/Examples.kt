package org.tameter.bones

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.greaterThan
import org.mockito.Mockito
import org.mockito.Mockito.any
import org.mockito.Mockito.atLeast

fun example_1() {
    object : Given {
        var prop = 3
    }.on {
        prop = 4
    }.then {
        assertThat(prop, equalTo(4))
    }
}

fun example_2() {
    data class D(val n: Int, val txt: List<String>)

    thisBlock<D> {
        object : Given {
            var prop = 3
        } on {
            prop--
        } then {
            verify {

            }
            prop iz greaterThan(n)
        }
    } applyToEach L(
        D(0, L("a", "b")),
        D(1, L("a", "c")),
        D(2, L("d", "c"))
    )
}

fun example_3() {
    data class D(val n: Int, val txt: List<String>)

    thisBlock<D> {
        object : Given {
            var prop = 3
            val myMock: MutableList<Int> = mock()
        } on {
            prop--
        } then {
            verify {
                myMock { size }
                2 of myMock { add(any_()) }
                atLeast(3) of myMock { add(any_()) }
            }
            verifyIt {
                myMock { it.size }
                2 of myMock { it.add(any()) }
                atLeast(3) of myMock { it.add(any()) }
            }
            prop iz greaterThan(n)
        }
    } applyToEach L(
        D(0, L("a", "b")),
        D(1, L("a", "c")),
        D(2, L("d", "c"))
    )
}
