package org.tameter.bones

import org.hamcrest.Matcher
import org.hamcrest.MatcherAssert.assertThat
import org.mockito.Matchers.any
import org.mockito.Mockito.mock
import org.mockito.Mockito.times as timesMode
import org.mockito.Mockito.verify
import org.mockito.verification.VerificationMode

interface Given

class GivenPlusStimulus<T : Given> (
    private val given: T,
    private val stimulus: T.() -> Unit
) {
    val givenSetup: T by lazy { given.apply(stimulus) }
}

infix fun <T : Given> T.on(actions: T.() -> Unit): GivenPlusStimulus<T> {
    return GivenPlusStimulus(this, actions)
}

inline infix fun <T: Given> GivenPlusStimulus<T>.then(block: T.() -> Unit) {
    givenSetup.run(block)
}

infix fun <T> T.iz(matcher: Matcher<T>) {
    assertThat(this, matcher)
}

// If we move to Mockito 2, could just use mockito-kotlin.
inline fun <reified T : Any> any_(): T = any(T::class.java)
inline fun <reified T : Any> mock(): T = mock(T::class.java)!!

@DslMarker
annotation class VerifyDsl

@VerifyDsl
class VerifyContext {
    operator fun <T> T.invoke(block: T.() -> Unit) {
        verify(this).block()
    }

    class CallOf<T>(private val mock: T, private val mode: VerificationMode) {
        operator fun invoke(block: T.() -> Unit) {
            verify(mock, mode).block()
        }
    }

    infix fun <T> VerificationMode.of(mock: T) = CallOf(mock, this)
    infix fun <T> Int.of(mock: T) = CallOf(mock, timesMode(this))
}

fun verify(block: VerifyContext.() -> Unit) {
    VerifyContext().run(block)
}

@VerifyDsl
class VerifyItContext {
    operator fun <T> T.invoke(block: (T) -> Unit) {
        block(verify(this))
    }

    class CallOf<T>(private val mock: T, private val mode: VerificationMode) {
        operator fun invoke(block: (T) -> Unit) {
            block(verify(mock, mode))
        }
    }

    infix fun <T> VerificationMode.of(mock: T) = CallOf(mock, this)
    infix fun <T> Int.of(mock: T) = CallOf(mock, timesMode(this))
}

fun verifyIt(block: VerifyItContext.() -> Unit) {
    VerifyItContext().run(block)
}

fun <T> thisBlock(block: T.() -> Unit) = block

infix fun <D> (D.() -> Unit).applyToEach(data: Iterable<D>) = data.forEach { this(it) }

@Suppress("FunctionName")
fun <T> L(vararg items: T) = listOf(*items)
